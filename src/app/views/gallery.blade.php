@extends('layouts.master')

@section('title')@parent:: Artwork Submissions @stop

@section('content')
<div class="container">
    <h1>Submitted Artwork</h1>
    <p>Below you will find the artwork submitted for episodes of the <a href="http://www.noagendashow.com">No Agenda Show</a>
    by the users of the Art Generator.</p>
    @if (Auth::user())
        <p><a href="{{ url('create') }}" class="btn btn-primary">Submit Artwork</a></p>
    @else
        <p><a href="{{ url('signup') }}">Create your account</a> to submit your own artwork.</p>
    @endif
    <div class="row fx">
    @foreach($artworks as $artwork)
        <div class="col-xs-6 col-md-3 img artworkwrapper {{{ isset($artwork->accepted_for->episode_number) ? 'selected' : '' }}}"
        @if (isset($artwork->accepted_for))
             data-accepted_for="{{{ $artwork->accepted_for->episode_number + 0 }}}" 
        @endif
        >
        @if (isset($artwork->accepted_for))
            <a href="/artwork/{{ $artwork->id }}"><div class="acceptedribbon"><span class="fa fa-star"></span> Ep. {{{ $artwork->accepted_for->episode_number + 0 }}} <span class="fa fa-star"></span></div></a>
        @endif
            <img
             class="artwork" src="{{ $artwork->path }}/{{ $artwork->filehash }}_thumbs/{{ $artwork->filehash }}_320.png"
             @if (isset($artwork->accepted_for))
                title="{{ $artwork->title }} - Accepted for Episode {{{ $artwork->accepted_for->episode_number + 0 }}},
                &ldquo;{{{ $artwork->accepted_for->title }}}&rdquo;"
             @else
                title="{{ $artwork->title }}"
             @endif
             >
             <div class="fx-overlay">
                <a href="/artwork/{{ $artwork->id }}" class="fx-expand"><span class="fa fa-search"></span></a>
                <a class="close-fx-overlay hidden">x</a>
                <div class="fx-info">
                    <h3>{{ $artwork->title }}</h3>
                    <h4>By {{{ $artwork->user->profile->name }}}</h4>
                    @if (isset($artwork->accepted_for))
                        <h4 class="acceptedlink"><a href="/episode/{{{ $artwork->accepted_for->id }}}">Album Art Selected for Episode {{{ $artwork->accepted_for->episode_number + 0 }}}</a></h4>
                    @endif
                </div>
             </div>
        </div>
        
    @endforeach
    </div>
</div>
<div class="container center">
	{{ $artworks->links() }}
</div>
@stop
