@extends('layouts.master')

@section('title')@parent:: {{{ $title }}} @stop

@section('content')
<div class="container">
    <h1>Accepted Artwork</h1>
    <p>Below you will find the accepted artwork and episode details for the <a href="http://www.noagendashow.com">No Agenda Show</a>.</p>
    <div class="row fx">
    @foreach($episodes as $episode)
        <?php $artwork = $episode->albumart($episode->artwork_id); ?>
        <div class="col-xs-6 col-md-3 img artworkwrapper">
            <img class="artwork"
            @if ($episode->artwork_id > 0)
                src="{{ $artwork->path }}/{{ $artwork->filehash }}_thumbs/{{ $artwork->filehash }}_320.png"
            @else
                src="/assets/img/artplaceholder320.jpg"
            @endif
                title="Episode {{ $episode->episode_number + 0 }} - &ldquo;{{ $episode->title }}&rdquo;"
                >
                <div class="fx-overlay">
                    <a href="{{ url('episode/' .  $episode->id) }}" class="fx-expand"><span class="fa fa-search"></span></a>
                    <a class="close-fx-overlay hidden">x</a>
                    <div class="fx-info">

                        <h3>&ldquo;{{{ $episode->title }}}&rdquo;</h3>
                        
                        <h4>Episode {{{ $episode->episode_number }}}
                            {{--@if ($episode->published)
                                
                                @if (is_object($artwork->user->profile))
                                    <br>Art Director: 
                                    {{{ $artwork->user->profile->name }}}
                                @endif
                            @endif
                            --}}
                        </h4>
                    </div>
                 </div>
        </div> 
    @endforeach
    </div>
</div>
<div class="container center">
	{{ $episodes->links() }}
</div>
@stop
