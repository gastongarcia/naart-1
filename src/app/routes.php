<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function () {
    return View::make('home');
});
//

Route::get('artworks', array('as' => 'gallery', 'uses' => 'ArtworkController@index'));
Route::get('evergreen', array('as' => 'evergreengallery', 'uses' => 'ArtworkController@evergreens'));
Route::post('editartwork', array('before' => 'auth', 'as' => 'editartwork', 'uses' => 'ArtworkController@edit'));
Route::post('deleteartwork', array('before' => 'auth', 'as' => 'deleteartwork', 'uses' => 'ArtworkController@delete'));
Route::get('artwork/{artwork}', array('as' => 'showartwork', 'uses' => 'ArtworkController@view'));
Route::get('create', array('before' => 'auth', 'as' => 'create', 'uses' => 'ArtworkController@create'));
Route::post('stash', array('before' => 'auth', 'as' => 'stash', 'uses' => 'ArtworkController@stash'));
Route::match(array('GET', 'POST'), 'crop', array('before' => 'auth', 'as' => 'crop', 'uses' => 'ArtworkController@crop'));
Route::post('process', array('before' => 'auth', 'as' => 'process', 'uses' => 'ArtworkController@process'));
Route::get('episodes', array('as' => 'episodelist', 'uses' => 'ArtworkController@episodeIndex'));
Route::get('episode/{episodeid}', array('as' => 'showepisode', 'uses' => 'ArtworkController@viewEpisode'));
Route::post('episode/update', array('before' => 'auth', 'as' => 'updateepisode', 'uses' => 'ArtworkController@updateEpisode'));
Route::get('artist/{artistid}', array('as' => 'artistprofile', 'uses' => 'ArtworkController@artistProfile'));
Route::post('editprofile', array('before' => 'auth', 'as' => 'updateprofile', 'uses' => 'UsersController@updateProfile'));

// Confide routes
//Route::get('signup', 'UsersController@login');
Route::post('users', 'UsersController@store');
Route::get('signup', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('logout', 'UsersController@logout');
Route::get('userspartial/{partial}', array('before' => 'auth', 'as' => 'userpartial', 'uses' => 'UsersController@userTypeahead'));
