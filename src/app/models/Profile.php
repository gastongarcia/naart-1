<?php

class Profile extends Eloquent
{
    protected $fillable = array('user_id', 'name', 'location', 'website');

    public function user()
    {
        return $this->belongsTo('User');
    }
}
