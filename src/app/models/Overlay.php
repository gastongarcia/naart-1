<?php

class Overlay extends Eloquent
{

    protected $guarded = array();

    public function artwork()
    {
        return $this->hasMany('Artwork');
    }

    public function user()
    {
        return $this->belongsTo('User');
    }

}
