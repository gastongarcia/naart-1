$(function(){
	$(document).on('click', '.artthumblink', function(e){
		e.preventDefault();
		if ($('#populateToggle').is(':checked'))
		{
			$('#acceptedartwork').val($(this).data('populateepisode'));
			$('#acceptedartwork').focus();
		} else {
			window.location.href = $(this).attr('href');
		}
	});
	$('#showdate').datetimepicker({
		pickTime: false
	});
});